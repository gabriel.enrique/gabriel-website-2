from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import MataKuliahForm, HapusForm
from .models import MataKuliah, HapusMatkul


def home(request):
    return render(request, 'main/home.html')

def story1(request):
    return render(request, 'main/story1.html')

def simplified(request):
    return render(request, 'main/simplified.html')

def detailed(request): 
    return render(request, 'main/detailed.html')

def matkul(request): 
    matkuls = MataKuliah.objects.all()
    response = {"matkuls" : matkuls}
    return render(request, 'main/matkul.html', response)

def view_matkul(request, id):
    matkul = MataKuliah.objects.get(id=id)
    response = {'matkul' : matkul}
    return render(request, 'main/view_matkul.html', response)

def add_matkul(request):
    response = {
        "form" : MataKuliahForm,
        'pesan' : ''
    }

    if request.method == "POST":
        form = MataKuliahForm(request.POST or None)
        if form.is_valid():
            form.save()
            response['pesan'] = 'Mata Kuliah berhasil ditambahkan'
            return render(request, 'main/add_matkul.html', response)
        else:
            response['pesan'] = 'Mata Kuliah gagal ditambahkan. Pastikan format masukkan sudah benar'
            return render(request, 'main/add_matkul.html', response)

    return render(request, 'main/add_matkul.html', response)
    
def hapus_matkul(request, id):
    matkul = MataKuliah.objects.get(id=id)
    matkul.delete()
    return HttpResponseRedirect("/matkul")