from django import forms
from .models import MataKuliah, HapusMatkul

class MataKuliahForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = ["nama", "semester_tahun", "dosen", "sks", "ruang_kelas", "deskripsi"]

    error_messages = {
        "required" : "Please Type"
    }

    input_attrs = {
        "nama" : {
            "type" : "text",
            "placeholder" : "PPW"
        },

        "semester tahun" : {
            "type" : "text",
            "placeholder" : "Gasal 2020/2021",
            "pattern" : "(Gasal|Genap) 20\d{2}\/20\d{2}"
        },

        "dosen" : {
            "type" : "text",
            "placeholder" : "Iis Afriyanti"
        },

        "sks" : {
            "type" : "number",
            "placeholder" : "3"
        },

        "ruang kelas" : {
            "type" : "text",
            "placeholder" : "A1.08 (Ged Baru)"
        },

        "desc" : {
            "placeholder" : "Write description here"
        }         
    }

    nama = forms.CharField(label="Nama Mata Kuliah", required=True, widget=forms.TextInput(attrs=input_attrs["nama"]))
    semester_tahun = forms.CharField(label="Semester / Tahun", required=True, widget=forms.TextInput(attrs=input_attrs["semester tahun"]))
    dosen = forms.CharField(label="Nama Dosen", required=True, widget=forms.TextInput(attrs=input_attrs["dosen"]))
    sks = forms.DecimalField(label="Jumlah SKS", required=True, widget=forms.TextInput(attrs=input_attrs["sks"]))
    ruang_kelas = forms.CharField(label="Ruang Kelas", required=True, widget=forms.TextInput(attrs=input_attrs["ruang kelas"]))
    deskripsi = forms.CharField(label="Deskripsi Singkat", required=True, widget=forms.Textarea(attrs=input_attrs["desc"]))



class HapusForm(forms.ModelForm):
    class Meta:
        model = HapusMatkul
        fields = ["nama"]

    error_messages = {
        "required" : "Please Type"
    }

    input_attrs = {
        "nama" : {
            "type" : "text",
            "placeholder" : "PPW"
        }       
    }

    nama = forms.CharField(label="Nama Mata Kuliah", required=True, widget=forms.TextInput(attrs=input_attrs["nama"]))