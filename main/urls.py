from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('story1', views.story1, name="story1"),
    path('simplified', views.simplified, name="simplified"),
    path('detailed', views.detailed, name="detailed"),
    path('matkul', views.matkul, name="matkul"),
    path('matkul/<int:id>', views.view_matkul),
    path('add_matkul', views.add_matkul, name="add_matkul"),
    path('hapus_matkul/<int:id>', views.hapus_matkul, name="hapus_matkul")
]
