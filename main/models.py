from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    nama = models.CharField(max_length=50)
    semester_tahun = models.CharField(max_length=20)
    dosen = models.CharField(max_length=50)
    sks = models.DecimalField(decimal_places=0, max_digits=1)
    ruang_kelas = models.CharField(max_length=50)
    deskripsi = models.TextField(max_length=100)

class HapusMatkul(models.Model):
    nama = models.CharField(max_length=50)